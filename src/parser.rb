require 'kramdown'

class Parser

	def initialize raw_text
		# md_content
		@md_content = raw_text
	end

	def get_nav_locals
		@nav_items = @md_content.scan(/\[nav\-item\]\((.*)\,\s(.*)\)/)
		self
	end

	def get_project_page_locals
		# title
		@title = match_first /\#(.+)/
		# subtitle
		@subtitle = match_first /\#\#\#(.+)/
		# background-img
		background_img = match_first /\[page\-img\-bg\]\((.+)\)/
		@background_img = "../img/articles-img/#{background_img}"
		self
	end

	def get_article_locals
		# title
		@title = match_first /\#(.+)/
		# subtitle
		@subtitle = match_first /\#\#\#(.+)/
		# img to show in article
		@article_img = match_first /\[article\-img\]\((.+)\)/
		# img as background of the article title
		@article_img_background = match_first /\[article\-img\-bg\]\((.+)\)/
		#livestream
		@livestream_link = match_first /\[livestream\-link\]\((.+)\)/
		# list of informative paragraphs shown in the article
		@article_infos = match_all /\[article\-info\]\s(.+)/
		# list of roleplay paragraphs shown in the article
		@article_roleplay_texts = match_all /\[article\-roleplay\-text\]\s(.+)/
		@learn_more_link = match_first /\[learn\-more\-link\]\((.+)\)/
		self
	end

	# change the md_content so that it can be parsed 
	# to kramdown md to html parser, returning html string
	def render
		@md_content.sub! /\[article\-img\]\(.+\)/, ""
		@md_content.sub! /\[article\-img\-bg\]\(.+\)/, ""
		@md_content.sub!  /\[livestream\-link\]\(.+\)/, ""
		@md_content.sub! /\[learn\-more\-link\]\(.*\)/, ""
		@md_content.sub! /\[nav\-item\]\(.*\)/, ""
		@md_content.gsub! /\[article\-info\].*/, ""
		@md_content.gsub! /\[article\-roleplay\-text\].*/, ""

		@md_content.gsub! /\[page\-img\-bg\]\(.*\)/, ""
		# gallery
		@md_content.gsub!(/\[gallery\]\((.+)\)/) {|n| "<div class='gallery'>" + render_gallery_images("#{$1}") + " </div>"}
		# pdf
		@md_content.gsub!(/\[pdf\]\((.+)\)/) {|n| "<div class='unloadedPDF' style='display:none;' pdfsrc='#{$1}'></div>"}
		#handle iframe
		@md_content.gsub!(/(<iframe.+>.*<\/iframe>)/) {|n| "<div class='unloadedIframe' style='display:none;' domelem='#{$1}'></div>"}

		Kramdown::Document.new(@md_content).to_html
	end

	def match_all regex
		@md_content.scan(regex).flatten
	end

	def match_first regex
		matched = @md_content.match(regex) || ""
		matched[1]
	end

	# custom markdown element, generate html structure 
	# for gallery markdown custom element
	def render_gallery_images dir_path

		#return a list of img in the slides directory
		path = File.join("**", dir_path[1..-1], "*")
		img_pathlist = Dir.glob path

		#sort the slides by their numbers
		sorted_img_pathlist = img_pathlist.sort_by { |img| img.match(/,*(\d+)/)[1].to_i }
		#filter only the list of slides, return a html of the slides
		sorted_img_pathlist.reduce("") do |gallery, img|
				gallery + "<img class='gallery-images' alt='gallery image' src='" + img.sub(/public/, "") + "'>"
			end
	end
	
	def to_hash
		hash = {}
		instance_variables.each do |var|
			var_value = instance_variable_get(var)
			hash[var.to_s.delete('@')] = var_value if var_value
		end
		hash.delete('md_content')
		hash
	end

end