require 'sinatra'
require 'haml'
require 'json'
require_relative 'render.rb'

class App < Sinatra::Base
	set :public_folder, 'public'
	set :views, 'views'
	
	set :global_locals, JSON.parse(File.read 'haml_locals.json')
	
	get '/' do
		render = Renderer.new 'markdown/'
		
		introduction_div = {introduction: haml(:introduction) }
		articles = render.homepage_article_locals
		nav = render.homepage_nav_list
		meta_and_footer = { 
			meta: haml(:meta, :locals => settings.global_locals),
			footer: haml(:footer, :locals => settings.global_locals)
		}

		locals = [
			introduction_div,
			articles, 
			nav, 
			settings.global_locals,
			meta_and_footer
		]
		
		haml(:home, :locals => locals.reduce(&:merge))
	end

	get '/:project_name' do
		project_list = Dir.glob('**', base: 'markdown/project-pages/').map {|filename| filename.sub /\.md/, ''}
		error 404 if !project_list.include?(params[:project_name])
		
		render = Renderer.new 'markdown/'

		project_content = render.project_content params[:project_name]
		nav = render.projectpage_nav_list
		meta_and_footer = { 
			meta: haml(:meta, :locals => settings.global_locals),
			footer: haml(:footer, :locals => settings.global_locals)
		}

		locals = [
			project_content, 
			nav,
			meta_and_footer
		]

		haml(:projects, :locals => locals.reduce(&:merge))
	end

	get '/content/:project_name' do
		project_list = Dir.glob('**', base: 'markdown/project-pages/').map {|filename| filename.sub /\.md/, ''}
		error 404 if !project_list.include?(params[:project_name])
		content_type :json
		render = Renderer.new 'markdown/'
		render.project_content(params[:project_name]).to_json
	end

	get '/image/og-img' do
		send_file File.expand_path('img/og_img.png', settings.public_folder),
			:type => "image/png",
			:disposition => 'inline'
	end

end