require_relative 'render.rb'

class Debugger

	def initialize
		@render = Renderer.new
		@additional_locals = {
			:nav_items => @render.nav_from(@render.homepage_dropdown_items),
			:slogan => 'Work for structure<br>Work for order<br>Patterns of Work',
			:page_description => 'Whatever for now'
		}
	end

	def log variable
		File.open('tmp/log.rb', 'w') do |f|
			f.puts variable
		end
	end

	def log_locals
		File.open('../tmp/locals.rb', 'w') do |f|
			f.puts @render.homepage_article_locals.merge(@additional_locals)
		end
	end

	def render_haml haml, locals
		haml = Haml::Engine.new File.read("../views/#{haml}.haml")
		haml.render Object.new, locals
	end

	def precompile
		locals = @render.homepage_article_locals.merge(@additional_locals)

		File.open('../tmp/precompiled.html', 'w') do |f|
			f.puts render_haml 'home', locals
		end
	end

end