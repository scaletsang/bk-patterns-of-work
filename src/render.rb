require 'haml'
require_relative 'parser.rb'

class Renderer 

	def initialize root = '../markdown/'
		@root = root
	end

	###########################
	### Home Page articles ####
	###########################

	def homepage_article_locals
		func = lambda do |filename, file_path|
			additional_locals = {
				article_id: filename
			}
			{filename.to_sym => render_article("#{@root + file_path}", additional_locals)}
		end
		return filenames_to_hash @root, 'home-articles/*' , func, 'home-articles/nav.md'
	end

	def render_article file_path, additional_locals = nil
		md_text = File.read file_path
		locals = Parser.new(md_text).get_article_locals.to_hash
		locals = locals.merge(additional_locals) if additional_locals
		render_haml 'home_article', locals
	end
	
	###########################
	# Home Page dropdown menu #
	###########################

	def nav_list file_path
		md_text = File.read file_path
		nav_list = Parser.new(md_text).get_nav_locals
		nav_list.to_hash
	end

	def homepage_nav_list
		nav_list 'markdown/home-articles/nav.md'
	end

	def projectpage_nav_list
		nav_list 'markdown/project-pages/nav.md'
	end

	###########################
	## Project Page articles ##
	###########################
	
	def project_content project_name
		md_text = File.read "#{@root}project-pages/#{project_name}.md"
		parser = Parser.new(md_text)
		project_content_locals = parser.get_project_page_locals.to_hash
		project_content_locals.merge({project_content: parser.render})
	end

	###########################
	#### Common functions #####
	###########################
	
	def filenames_to_hash dir, regex, func, exclude
		files = Dir.glob(regex, base: dir)
		files.delete(exclude)
		result = files.reduce({}) do |hash, filename|
			filename_in_sub_dirs = filename.match(/.*\/(.*)\.md/)
			hash_k = filename_in_sub_dirs ? filename_in_sub_dirs[1] : filename.sub(/\.md/, '')
			hash.update func.call(hash_k, filename)
		end
		result
	end

	def titlize string, seperator
		uncapitalized_words = ['the', 'in', 'and', 'of']
		string_list = string.downcase.split(seperator)
		head = string_list.delete_at(0).capitalize
		
		rest = string_list.map do |word|
			uncapitalized_words.include?(word) ?
				word : word.capitalize
		end
		[head].concat(rest).join(' ')
	end

	def render_haml haml_name, locals
		haml = Haml::Engine.new File.read("views/#{haml_name}.haml")
		haml.render Object.new, locals
	end

end