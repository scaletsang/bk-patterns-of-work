# Levitation

### by Olivier van den Brandt & Carlos Gonzalez Morenoe

[page-img-bg](Placeholder.jpg)

This productions comes from conversations between Olivier and other citizens during their restructuration programs. Normally, we help our citizens towards the right path in their journey to rejoin our society - but here we decided to make an exception. Olivier was talking about how architecture and space could partake in new roles.

Relations between floating and disconnecting yourself from everything one is connected with. From the very laws of nature to the references you build up in the world. Olivier proclaims "The elements that strongly determine your connections and place with your spatial environment are the relationships between rest & danger; displacement & time; accelerated & lost feel of time; consequences of your action & the lack of action; being observed or not or even the thought of what you will leave behind. How these relationships relate, influence the subconscious when experiencing space and your own reality in it. Architecture and form can help us ground ourselves and our feelings with the now".

Though this way of thinking would put one into our restructuration program immediately, the POW has decided to invite Olivier and his friend Carlos to experiment with these thoughs in our office space as one of the 5 productions during our programming. We at the BK-POW are intruiged, wondering whether this old and chaotic way of thinking can help us find answers to restructuring our architecture to further staple order, structure and wellbeing into our society.

~ BK-POW