# Everything Personal

### by Stef Fridael

[page-img-bg](EverythingPersonalPage.jpg)

The production Everything Personal by citizen Stef Fridael is a circular work in relation to the old phenomenon of “Hoarding Disorder”. It is intended to involve the viewer in the emotional sides of this phenomenon. Stocking up on food is now a well-known concept due to the Corona pandemic in regions outside of the POWs reach. However, hoarding goes a step further.

Now you as citizen of the POW can take a closer look into the mind of a citizen abstaining from our beliefs. The items hoarded will reflect how some citizens spiral downwards when rejecting the pillars or structure and order. It is in small steps, were one first holds on to certain "possessions" out of fear or disbelief. Only to later succumb into addiction of collecting and prepping for unimaginable and unreal fantasies. Fantasies which will never be able to occur in our society.

It is for this reason we have allowed citizen Stef Fridael to share his past hoarding tendecies and show the rest of us why it is important, to uphold our own roles in society and work together towards structurizing how we live.

~ BK-POW