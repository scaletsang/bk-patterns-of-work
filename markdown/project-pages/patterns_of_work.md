#  Patterns of Work

### by Gallery Alte Feuerwache Loschwitz & Holger Wendland

[page-img-bg](PatternsofWorkPage.jpg)

After witnessing how this group of citizens organized their previous "Patterns of Work" productions, we at the POW contacted them to set up for this program. They believed that for a long time, the world of work was not the focus of the world. In their own words:

"If at all, the world only dealt with the extreme aspects of labour exploitation in developing countries. For some years now, however, this has changed. The value of labour, crafts, the working environment, the results and consequences of the industrial exploitation of the earth are more and more in the focus of interest. Here, the artists develop their own yardstick for assessing the subject of work, they bring their philosophy into play, sometimes developing ironic or comical positions. The approach to the poster production "PATTERNS OF WORK" is broad, open and international: The artists opened up perspectives on the past, the present and the future of work. For work and worlds of work were and are part of people's global everyday lives, they determine the self-image of the working subject. At the same time, work and worlds of work could not be more different in their constitution and structure. These global worlds of work were and are also intertwined, even though the concept and understanding of what work actually is has changed again and again - a process that is still not complete today". 

Though we do not see eye to eye to certain aspects of these citizens, we do agree on how society should value work more than just means of labour. It is a way of living, a way to achieve a strong coherent society in which work unifies each citizen. As POW, we highlight this thought by presenting these posters for the production during the our whole programming period.

~ BK-POW