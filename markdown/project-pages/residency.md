# 24 Hours in The Cave

### Residency - open for applications

[page-img-bg](Placeholder.jpg)

Work is beauty. The traces it leaves behind indicating we as humans can structure our lives and work towards a common goal. It is the pillar of our society. Therefore, we believe that work should be a priority for our citizens. Everyone has their own particular role that they carry out for their whole life. Everyone is a cog in the wheel, a part of a greater system that runs smoothly when all parts work together. It is from this appreciation and preservation of work that the BK-POW has initiated *24 hours in The Cave*.

24 Hours in The Cave is one of our 5 productions during our program "Patterns of Work". There are citizens that participate in chaotic and unruly behaviour, wanting to “create” something out of what they “have” instead of working within their own role. We would normally enroll them into our restructuration service. But now, the POW has lended us the opportunity to enroll these citizens into this new production, using their deranged views to seek answers to how our citizens can fall victim to chaos and disorder. And how we as BK-POW can learn from this so we can report to the great POW on how to strengthen ourselves as society to uphold our impeccable order, structure and wellbeing.

But we also believe that our citizens might be giving into temptation for a reason. Where we have sustained our society for many many years, maybe some form of adaptation can benefit our world. Chaos and disorder seem to come back time after time. Is it there for a reason and why does it persist in its presence? We aim for The Cave to help us seal the cracks from which chaos thrives.

~ BK-POW
