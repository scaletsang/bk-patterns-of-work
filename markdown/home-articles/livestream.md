# Livestream 

### From our office space in T56

[livestream-link](https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2Fpatternsofwork21%2Fvideos%2F401404718328783%2F&show_text=false&width=560&t=0)

[article-info] We invite you to witness our program, either through our online stream above or during one of the 5 show moments at our office in T56. Chairman Pigé will personally announce each production and give an opening speech - an honour to experience in person.

[article-roleplay-text] As regional BK-POW, we aim to inform, structure, and handle all inquiries, to represent the impeccable social fabric of our region. Recently all BKs have been given authority to initiate their own programs. Starting on the 11th of September, our chosen Chairman Pigé has curated a visionary selection of activities to be held in our office space at T56 in Eindhoven, The Netherlands. Over a period of 3.5 months, a number of productions will be performed in relay form during 5 show moments. 