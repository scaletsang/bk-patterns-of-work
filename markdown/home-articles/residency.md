# 24 Hours in The Cave

### Residency - open for applications

[article-img-bg](ResidencyCard.jpg)

[article-info] A cave will be built in our office hall at T56 and equipped with infrastructure and materials to produce work. During a period of one month, 8 participants will be invited by BK-POW to use this cave for 24 hours. Their work will be passed on to the next participant in relay form after the 24 hour time slot. The result will be one finished work which has been passed on from participant to participant. The work and accompanying research done by BK-POW will be published and shown during our production. All activities inside The Cave will be shown through a live stream - so you can see the progress of work unravel.

[article-roleplay-text] BK-POW is looking for 8 artists to carry out this assignment. You can apply now using the information below.

[article-roleplay-text] +  Starting date: 11th of September 2021<br>+  8 24 hour slots: 11, 15, 18, 22, 25, 29 September & 02, 06 October <br>+  Application deadline: 20th of August 2021, 23:59<br>+  Artist compensation: €100 reward + meals & travel expenses<br>+  Procedure: Selection on the 27th of August 2021

[article-roleplay-text] Send an Email titled “Registration 24h In the Cave” with your resume and motivation us at bkpatternsofwork@gmail.com

[learn-more-link](/residency)