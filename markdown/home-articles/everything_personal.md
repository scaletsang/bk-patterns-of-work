# Everything Personal

### by Stef Fridael

[article-img](EverythingPersonalCard.jpg)

[article-info] With this production, a performance will be given to the public in which citizen Stef Fridael collects material on location, or has collected it, sorts this material and then processes it into an installation. At the end of an exhibition period, the installation will have grown to excessive proportions, after which it will be dismantled and the material will flow back into its environment. This creates a circular performance in which interfaces are questioned about our industry and our behavior.

[article-roleplay-text] Though now absent in our society, before our reign as the POW famine and scarcity were known concepts for humanity. We invited this citizen to remind us of how a world with no structure and efficiency will derail into failing to provide for its own habitants.

[learn-more-link](/everything_personal)