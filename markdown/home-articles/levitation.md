# Levitation

### by Olivier van den Brandt & Carlos Gonzalez Moreno

[article-img](LevitationCard.jpg)

[article-info] This production by citizens Olivier and Carlos will focus on using space and architecture in a way we normally abstain from. Where space and structure tell us where to work and how to function, these two citizens will us our office space to let other citizens experience feelings of consequence and time, as well as experience the relationship with yourself, others and the space you are in.

[article-roleplay-text] There is no place for aberrant views in our society. Certainly not in how we view our spaces for working and living. For this program, in particular this production from these two citizens, we believe we can utilize these attempts at finding different uses for architecture to better our own buildings and structures. So you can experience the greatness of our own developed architecture even better, everywhere you enter one of our facilities.

[learn-more-link](/levitation)