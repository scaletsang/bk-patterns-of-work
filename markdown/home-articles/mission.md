# Mission

### Order. Structure. Wellbeing.

[article-img-bg](MissionCard.jpg)

[article-info] As the regional right hand of the POW, we at the BK-POW are dedicated to make sure you as citizens are provided with the right communication and help to reap the benefits of our beautiful society. Our mission doesn’t stop there. We are also here to sense abnormalities that may rise within our citizens. We aim to seek these out and provide the necessary restructuration to reconnect these citizens.

[article-roleplay-text] It is from this perspective that the POW has allowed us to carry out the program “Patterns of Work”. We will take full organizational power during this program. Each of the selected productions provide us with different views on the start, the influences and the consequences of the very things we aim to terminate: chaos, disorder and  uncertainty.
[article-roleplay-text] ~ BK-POW

[page-img-bg](MissionPage.jpg)

*“As Chairman of this region’s BK, I am honoured to have been chosen by the POW to take leadership of this region and our upcoming program. This program will serve as a probe, finding new ways to ensure that our impeccable order, structure and wellbeing can continue to thrive. So we as a region and as a nation can continue to rule out chaos and uncertainty together”.*
~ Chairman Pigé

Society should be static and unchangeable, without a cloud in the sky. We at POW believe the only way of living is following order and structure to achieve wellbeing through one, centralized power. From the moment we established ourselves, we had in mind to tackle the chaos and the uncertainty that had this world in shambles. We had to step in. We had to create an ideal state of being, one in which safety and security are our top priorities for our citizens.

As POW, we have provided answers to the many riddles that this world threw at us. Dedicated to a systematic approach to keep our citizens in their own roles, our buildings built to support work and our values focused on being one oiled machine working towards eradicating chaos and uncertainty from this world - for once and forever. 

This program is an innovative probe to examine how chaos starts from unseeable cracks in society. We fully support this initiative from the BK-POW as we believe to fully fight chaos, one must understand what chaos is.

For Order
For Structure
For Wellbeing

~ POW