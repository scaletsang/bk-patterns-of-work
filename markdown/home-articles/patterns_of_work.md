#  Patterns of Work

### by Gallery Alte Feuerwache Loschwitz & Holger Wendland

[article-img](PatternsofWorkCard.jpg)

[article-info] The first production on which the POW has been allowed to expand to a total of 5 productions, “Patterns of Work” is a celebration of the beauty of work. Our citizens from different regions have put their hands together to make visual snippets of their work, their very own role in our society, for all other citizens to see. 50 citizens from over 22 different regions have joined for this production, their work can be seen in our office space during our whole program.

[article-roleplay-text] At the POW, we have always stated that work is one of the main pillars of our society. When each and every one of our citizens work towards upholding our way of living, we can achieve the perfect machine. This production is a celebration of this ideal state where each citizen not only executes their role perfectly, but also enjoys being the specific cog in the greater wheel that is our nation.

[learn-more-link](/patterns_of_work)