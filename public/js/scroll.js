import polyfill from './smoothscroll.js'

const main = document.querySelector('main')
const swipeDownBtn = document.querySelector('#swipe-down-btn')

//////////////////////////////////////////
//// Codes that make smooth scrolling ////
//// when clicking on the nav items. /////
//////////////////////////////////////////

export default function smoothScrollToTarget(targetElement) {
	const elementTop = targetElement.getBoundingClientRect().top
	const offsetTop = window.pageYOffset
	const buffer = 50
	const top = elementTop + offsetTop - buffer
	
	main.scroll({
		top,
		behavior: "smooth"
	})
}

export function smoothScrollInit() {
	// kick off the polyfill!
	polyfill()
}

export function attachSwapDownBtnEventListener() {
	swipeDownBtn.addEventListener('click', event => {
		const targetElement = main.children[1]
		smoothScrollToTarget(targetElement)
	})  
}