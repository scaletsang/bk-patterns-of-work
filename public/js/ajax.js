import titlize from './titlize.js'

const article = document.querySelector('article')

export default function ajaxPageContent(navItem) {
	const url = navItem.getAttribute('href')
	getProjectContent(`/content${url}`).then( responseJson => {
		article.innerHTML = responseJson['project_content']
		document.body.style.backgroundImage = `url(${responseJson['background_img']})`
	})

	history.replaceState({'page': url}, `page ${url.substring(1)}`, url.substring(1))
	changeNavName()
}

async function getProjectContent(url) {
	// Default options are marked with *
	let response = await fetch(url, {
		method: 'GET', // *GET, POST, PUT, DELETE, etc.
		mode: 'same-origin', // no-cors, *cors, same-origin
		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		referrerPolicy: "no-referrer",
		credentials: 'same-origin', // include, *same-origin, omit
		headers: {
			'Content-Type': 'application/json'
			// 'Content-Type': 'application/x-www-form-urlencoded',
		}
	})
	return response.json()
}

let currentNavItem
export function changeNavName() {
	const navCurrentarticle = document.getElementById('article-name')
	navCurrentarticle.innerHTML = titlize(window.location.pathname.substring(1), '_')
	currentNavItem?.classList.remove('nav-is-viewing')
	currentNavItem = document.querySelector(`nav .nav-dropdown a[href$='${window.location.pathname}']`)
	currentNavItem.classList.add('nav-is-viewing')
}

export function backToHomepage(navItem) {
	const url = navItem.getAttribute('href')
	if (url === '/home') window.location.assign('/')
}