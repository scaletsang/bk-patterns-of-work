import navBarInit, {changeNavDisplayOnResize} from './nav.js'
import ajaxPageContent, {changeNavName, backToHomepage} from './ajax.js'

changeNavDisplayOnResize()
window.onload = () => {
	changeNavName()
	navBarInit((event, navItem) => {
		event.preventDefault()
		backToHomepage(navItem)
		ajaxPageContent(navItem)
	})
}