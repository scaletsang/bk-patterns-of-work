function capitalize(s) {
	return (s && s[0].toUpperCase() + s.slice(1)) || ""
}

export default function titlize(str, seperator) {
	const uncapitalizedWords = ['the', 'in', 'and', 'of']
	const stringList = str.toLowerCase().split(seperator)
	let newStringList = stringList.map( word => {
		return uncapitalizedWords.includes(word) ?
				word : capitalize(word)
	})
	return capitalize(newStringList.join(' '))
}