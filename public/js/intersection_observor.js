import titlize from "./titlize.js"

const main = document.querySelector('main')
const articles = main.querySelectorAll('article')
const footer = main.querySelector('footer')
const navCurrentarticle = document.getElementById('article-name')
const navMenu = document.querySelector('nav .nav-dropdown')

export default function changeNavNameOnScroll() {
	setIntersectionObserverOnArticles( entry => {
		const currentNavItem = navMenu.querySelector(`a[href$='#${entry.target.id}']`)

			if (entry.isIntersecting) {
			  navCurrentarticle.innerHTML = titlize(entry.target.id, '_')
				currentNavItem?.classList.add('nav-is-viewing')
				return
			}
			currentNavItem?.classList.remove('nav-is-viewing')
	})
}

// Change nav text according to the scrolling area
export function setIntersectionObserverOnArticles(callback) {
	const option = {
		root: null,
		threshold: 0.15,
		rootMargin: '-15px'
	}
	
	const observer = new IntersectionObserver( (entries, observer) => {
		entries.forEach( entry => {
			callback.call(this, entry)
		})
	}, option)
	
	articles.forEach( article => observer.observe(article))
	observer.observe(footer)
}