import changeNavNameOnScroll from './intersection_observor.js'
import navBarInit, {changeNavDisplayOnResize} from './nav.js'
import smoothScrollToTarget, {smoothScrollInit, attachSwapDownBtnEventListener} from './scroll.js'
import resizeLivestream, {resizeLivestreamOnResize} from './livestream_resize.js'

changeNavDisplayOnResize()
resizeLivestreamOnResize()

window.onload = () => {
	smoothScrollInit()
	attachSwapDownBtnEventListener()
	navBarInit((event, dropdownItem) => {
		const target = event.target
		const targetId = target.hash
		const targetElement = document.querySelector(targetId)
		smoothScrollToTarget(targetElement)
	})
	changeNavNameOnScroll()
	resizeLivestream()
}