const main = document.querySelector('main')
const nav = document.querySelector('nav')
const navMenu = nav.querySelector('.nav-dropdown')
const navBar = nav.querySelector('#nav-bar')
const navMenuBtn = document.getElementById('menu-button')
const navMenuItems = navMenu.querySelectorAll('.nav-dropdown > a')
let isDesktopView = () => window.innerWidth >= 1000

export default function navBarInit(dropdownItemOnClickEvent) {
	// Menu toggle button
	document.querySelector(':root').style.setProperty('--nav-items-count', navMenu.children.length)

	navMenuItems.forEach( dropdownItem => {
		dropdownItem.addEventListener('click', event => {
			toggleDropdownAnimation()
			dropdownItemOnClickEvent(event, dropdownItem)
		})
	})

	navMenuBtn.addEventListener('click', event => {
		toggleDropdownAnimation()
	})

	// Using polyfill for smooth scrolling in older browsers
	main.addEventListener('scroll', event => {
		if (navToggled) toggleDropdownAnimation()
	})
}

export function changeNavDisplayOnResize() {
	// For desktop width
	if (isDesktopView()) {
		navBar.appendChild(navMenu)
	}

	window.addEventListener('resize', event => {
		if (isDesktopView()) {
			navBar.appendChild(navMenu)
			navMenu.style.opacity = 1
		} else {
			nav.appendChild(navMenu)
		}
	})
}

//////////////////////////////////////////////////
// Codes that make nav bar drop down animation ///
//////////////////////////////////////////////////

export let navToggled = false
export function toggleDropdownAnimation() {
	if(isDesktopView()) return
	const time = 500
	const dropdownItems = navMenu.children
	const count = dropdownItems.length
	const timePerStep = time / count

	if (navToggled) {
		hideDropdown(time, dropdownItems, count, timePerStep)
		navToggled = false
		return
	}

	showDropdown(time, dropdownItems, count, timePerStep)
	navToggled = true
}

function showDropdown(time, dropdownItems, count, timePerStep) {
	navMenu.style.opacity = 1
	for (let i = 0; i < count; i++) {
		const step =  i + 1
		const bottomPercentage = step * 100
		setTimeout(() => {
			for (let k = i; k < count; k++) {
				dropdownItems[k].style.bottom = `${bottomPercentage}%`
			}
		}, timePerStep * step)
	}
}

function hideDropdown(time, dropdownItems, count, timePerStep) {
	for (let i = count - 1; i >= 0; i--) {
		const step = i + 1
		const bottomPercentage = i * 100
		setTimeout(() => {
			for (let k = i; k < count; k++) {
				dropdownItems[k].style.bottom = `${bottomPercentage}%`
			}
		}, timePerStep * ((-step) + count + 1))
	}
	setTimeout(() => {
		navMenu.style.opacity = 0
	}, time + timePerStep)
}