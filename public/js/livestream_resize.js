const livestream = document.getElementById('livestream-embed')

export function resizeLivestreamOnResize() {
  window.addEventListener('resize', event => {
    resizeLivestream()
  })
}

export default function resizeLivestream() {
  livestream.style.height = livestreamSize() + 'vh'
}

function livestreamSize() {
  const windowSize = window.innerWidth
  if (windowSize > 800) return '53'
  if (windowSize > 560) return '45'
  if (windowSize > 400) return '35'
  return '30'
}