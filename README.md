# How to update content?

All the website contents are in the folder called 'markdown'. It only contains markdown files. Inside, there are 2 folders:
+ the 'home-articles' folder contains markdown files describing each articles in the home page. If you want more articles, then just add a markdown file here.
+ The 'project-pages' folder contains markdown files describing each project pages. The name of each markdown file correspond to the url of that page. e.g. levitation.md will generate a page whose url is 'www.example.com/levitation'. If you want to add new pages, just add a markdown file here, easy!

[[_TOC_]]

# How to write Markdown?

## Basic syntax

### Normal Paragraph

```markdown
Any paragraph is written like this, without any format, and each paragraph is seperated by a new line.

Second paragraph...
```

### *Italic text*:

```markdown
*Italic texts*
```

### **Bolded text**:

```markdown
**Bolded texts**

```

### Insert Images

```markdown
![text_to_be_shown_if_the_photo_cannot_be_loaded](/img/example.jpg)

```

### Insert hyperlinks

```markdown
[example](http://www.google.com)
```

will generate [example](http://www.google.com)

### Insert ordered list

```markdown
1. first list item
2. second list texts
3. bla bla bla
  1. hihi
  2. bye bye

```

### Insert unordered list

```markdown
+ bullet list first item
+ bullet list second whatever
  + nested bullet list

```

## Syntax specifically for this website

These are additional keywords and its corresponding syntax that you can use when writing the content in a markdown document.

```markdown
[article-img](example.jpg)
[article-img-bg](example.jpg)
[livestream-link](https://www.youtube.com/embed/aam6vavhro4)

[article-info] An informative paragraph about the current article.
[article-roleplay-text] A role play paragraph, more artistic description about the current article.

[page-img-bg](example.jpg)
[learn-more-link](/link)

[gallery](/folder-name-containing-all-the-images)
* The path can only be a folder containing gallery images
* Each image must be named with a number at the end, other parts of the name must not contain any number. The number will be used to determine how it will be ordered in the gallery view. These are all valid names: IloveMyself1.jpg, whatever%#*#hi2.jpg, normal_name3.jpg

[pdf](example.pdf)
```

# Template for writing a article:

```markdown

# Title
### Subtitle

[article-img](article-image.jpg)
[article-img-bg](article-background-image.jpg)

[article-info] This is an informative paragraph about this article.

[article-roleplay-text] This is a role play paragraph, more artistic description about this article.

[learn-more-link](/example)

```

# Template for writing a project page:

```markdown

# Title
### Subtitle

[page-img-bg](background-image.jpg)

This is the first paragraph on the page.

More contents...

```

# Writing a nav menu:

```markdown

[nav-item](text, text link)

```